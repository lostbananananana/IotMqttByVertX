package com.fsmart.iot.starter;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.AsyncMap;
import io.vertx.core.shareddata.SharedData;
import io.vertx.ext.web.Router;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WebVerticle extends AbstractVerticle {


  private final Logger LOGGER = LogManager.getLogger(WebVerticle.class);

  @Override
  public void start(Promise<Void> startPromise){
    HttpServer server = vertx.createHttpServer();
    EventBus eb = vertx.eventBus();
    SharedData sharedData = vertx.sharedData();
    Router router = Router.router(vertx);

    router.route("/createProduct").handler(routingContext -> {

      HttpServerRequest httpServerRequest = routingContext.request();
      httpServerRequest.bodyHandler(bufferAsyncResult -> {

        JsonObject jsonObject = bufferAsyncResult.toJsonObject();
        String productName = jsonObject.getString("productId");
        String action = jsonObject.getString("action");
        HttpServerResponse response = routingContext.response();
        response.putHeader("content-type", "text/plain");

        if(action == null || action.isEmpty()) {
          response.end("failed");
        }else if(action.equals("create")) {
          eb.request("product-create", productName, res->{
            if(res.succeeded()){
              response.end("success");
            }else{
              response.end("failed");
            }
          });
        }else if(action.equals("delete")){
          eb.request("product-delete", productName, res->{
            if(res.succeeded()){
              response.end("success");
            }else{
              response.end("failed");
            }
          });
        }
      });
    });

    router.route("/sendOrder").handler(routingContext -> {
      HttpServerRequest httpServerRequest = routingContext.request();
      httpServerRequest.bodyHandler(bufferAsyncResult -> {

        JsonObject jsonObject = bufferAsyncResult.toJsonObject();
        String deviceSecret = jsonObject.getString("deviceSecret");
        String order = jsonObject.getString("order");
        HttpServerResponse response = routingContext.response();
        response.putHeader("content-type", "text/plain");

        sharedData.<String, String>getAsyncMap("mqtt-client-devicesecret-clientIdentifier", res -> {
          if (res.succeeded()) {
            AsyncMap<String, String> map = res.result();
            LOGGER.debug("share data create mqtt-client-topic success");
            map.get(deviceSecret, resput->{
              LOGGER.debug("get data success " + resput.result());
              eb.request(resput.result(), order, reseb -> {
                if (reseb.succeeded()) {
                  response.end("success");
                } else {
                  response.end("failed");
                }
              });
            });
          } else {
            // Something went wrong!
            LOGGER.error("share data create mqtt-client-topic failed");
          }
        });
      });
    });

    server.requestHandler(router).listen(config().getInteger("http.port"), ar->{
      if(ar.succeeded()){
        startPromise.complete();
      }
      else{
        startPromise.fail(ar.cause());
      }
    });
  }


}
