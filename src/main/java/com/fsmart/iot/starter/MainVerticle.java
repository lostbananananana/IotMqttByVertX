package com.fsmart.iot.starter;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class MainVerticle extends AbstractVerticle {

  private final Logger LOGGER = LogManager.getLogger(MainVerticle.class);
  @Override
  public void start(Promise<Void> startPromise) throws Exception {

    int port = config().getInteger("http.port");
    String redisPath = config().getString("redis.path");
    String kafkaServer = config().getString("kafka.bootstrap.servers");
    int mqttport = config().getInteger("mqtt.port");
    String mqttUrl = config().getString("mqtt.url");
    vertx.deployVerticle(new RedisVerticle(),  new DeploymentOptions().setConfig(new JsonObject().put("redis.path", redisPath)), re->{
      if(re.succeeded()){
        vertx.deployVerticle(new MqttServerVerticle(), new DeploymentOptions().setConfig(new JsonObject().put("kafka.bootstrap.servers", kafkaServer)
          .put("mqtt.port", mqttport)
          .put("mqtt.url", mqttUrl)), ar->{
          if(ar.succeeded()){
            vertx.deployVerticle(new WebVerticle(), new DeploymentOptions().setConfig(new JsonObject().put("http.port", port)), redisRe->{
              if(redisRe.succeeded()){
                vertx.setTimer(10000, id ->{
                  vertx.deployVerticle(new MqttClientVerticle());
                  vertx.deployVerticle(new MqttClientVerticle2());
                });
                startPromise.complete();
              }else{
                startPromise.fail(redisRe.cause());
              }
            });
          }else{
            startPromise.fail(ar.cause());
          }
        });
      }else{
        startPromise.fail(re.cause());
      }
    });


  }
}
