package com.fsmart.iot.starter;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.kafka.admin.KafkaAdminClient;
import io.vertx.kafka.admin.NewTopic;
import io.vertx.kafka.admin.TopicDescription;
import io.vertx.kafka.client.common.TopicPartitionInfo;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.Properties;

public class KafkaAdminUtilVerticle extends AbstractVerticle {

  private final Logger LOGGER = LogManager.getLogger(KafkaAdminUtilVerticle.class);

  @Override
  public void start(Promise<Void> startPromise) throws Exception {
    EventBus eb = vertx.eventBus();

    Properties config = new Properties();
    config.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.30.32:9092");

    KafkaAdminClient adminClient = KafkaAdminClient.create(vertx, config);

    MessageConsumer<String> consumerCreate = eb.consumer("product-create");
    consumerCreate.handler(message -> {
      LOGGER.debug("received a message: " + message.body());
      adminClient.createTopics(Collections.singletonList(new NewTopic(message.body(), 1, (short)1)), ar -> {
        // check if they were created successfully
        if(ar.succeeded()){
          LOGGER.debug("success");
        }else{
          LOGGER.debug(("failed"));
        }
      });
    });


    MessageConsumer<String> consumerDelete = eb.consumer("product-delete");
    consumerDelete.handler(message -> {
      LOGGER.debug("received a message: " + message.body());
      adminClient.deleteTopics(Collections.singletonList(message.body()), ar -> {
        if(ar.succeeded()){
          LOGGER.debug("success");
        }else{
          LOGGER.debug(("failed"));
        }
      });
    });

    adminClient.describeTopics(Collections.singletonList("test"), ar -> {
      TopicDescription topicDescription = ar.result().get("test");

      LOGGER.debug("Topic name=" + topicDescription.getName() +
        " isInternal= " + topicDescription.isInternal() +
        " partitions= " + topicDescription.getPartitions().size());

      for (TopicPartitionInfo topicPartitionInfo : topicDescription.getPartitions()) {
        LOGGER.debug("Partition id= " + topicPartitionInfo.getPartition() +
          " leaderId= " + topicPartitionInfo.getLeader().getId() +
          " replicas= " + topicPartitionInfo.getReplicas() +
          " isr= " + topicPartitionInfo.getIsr());
      }
    });

  }

}
