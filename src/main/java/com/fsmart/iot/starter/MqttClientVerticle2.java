package com.fsmart.iot.starter;

import io.netty.handler.codec.mqtt.MqttQoS;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.mqtt.MqttClient;
import io.vertx.mqtt.MqttClientOptions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.charset.Charset;
import java.util.Date;

public class MqttClientVerticle2 extends AbstractVerticle {

  private final Logger LOGGER = LogManager.getLogger(MqttClientVerticle2.class);

  private static final String MQTT_TOPIC = "/xxxx";
  private static final String MQTT_MESSAGE = "Hello Vert.x MQTT Client";
  private static final String BROKER_HOST = "localhost";
  private static final int BROKER_PORT = 1883;


  @Override
  public void start()  {
    MqttClientOptions options = new MqttClientOptions().setKeepAliveTimeSeconds(2).setUsername("productId").setPassword("xxxx2");


    MqttClient client = MqttClient.create(Vertx.vertx(), options);


    // handler will be called when we have a message in topic we subscribing for
    client.publishHandler(publish -> {
      LOGGER.debug("Just received message on [" + publish.topicName() + "] payload [" + publish.payload().toString(Charset.defaultCharset()) + "] with QoS [" + publish.qosLevel() + "]");
    });

    // handle response on subscribe request
    client.subscribeCompletionHandler(h -> {
      LOGGER.debug("Receive SUBACK from server with granted QoS : " + h.grantedQoSLevels());

      // let's publish a message to the subscribed topic
      vertx.setPeriodic(15000, ar->{
        client.publish(
          MQTT_TOPIC,
          Buffer.buffer(MQTT_MESSAGE+new Date()+" 2"),
          MqttQoS.EXACTLY_ONCE,
          false,
          false,
          s -> LOGGER.debug("Publish sent to a server"));
      });


      // unsubscribe from receiving messages for earlier subscribed topic
      //vertx.setTimer(5000, l -> client.unsubscribe(MQTT_TOPIC));
    });

    // handle response on unsubscribe request
    client.unsubscribeCompletionHandler(h -> {
      LOGGER.debug("Receive UNSUBACK from server");
      vertx.setTimer(5000, l ->
        // disconnect for server
        client.disconnect(d -> LOGGER.debug("Disconnected form server"))
      );
    });


    client.pingResponseHandler(s -> {
      //The handler will be called time to time by default
      LOGGER.debug("We have just received PINGRESP packet");
    });


    // connect to a server
    client.connect(BROKER_PORT, BROKER_HOST, ch -> {
      if (ch.succeeded()) {
        LOGGER.debug("Connected to a server");

        client.publishCompletionHandler(id -> {
          LOGGER.debug("Id of just received PUBACK or PUBCOMP packet is " + id);
        });
        // The line of code below will trigger publishCompletionHandler (QoS 2)
        //client.publish("my_topic", Buffer.buffer("hello"), MqttQoS.EXACTLY_ONCE, false, false);
        // The line of code below will trigger publishCompletionHandler (QoS is 1)
        //client.publish("my_topic", Buffer.buffer("hello"), MqttQoS.AT_LEAST_ONCE, false, false);
        // The line of code below does not trigger because QoS value is 0
        //client.publish("my_topic", Buffer.buffer("hello"), MqttQoS.AT_LEAST_ONCE, false, false);


        client.subscribe(MQTT_TOPIC, 0);
      } else {
        LOGGER.debug("Failed to connect to a server");
        LOGGER.debug(ch.cause());
      }
    });
  }

}
