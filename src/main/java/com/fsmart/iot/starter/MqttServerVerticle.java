package com.fsmart.iot.starter;

import io.netty.handler.codec.mqtt.MqttQoS;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.shareddata.AsyncMap;
import io.vertx.core.shareddata.SharedData;
import io.vertx.kafka.client.producer.KafkaProducer;
import io.vertx.kafka.client.producer.KafkaProducerRecord;
import io.vertx.kafka.client.producer.RecordMetadata;
import io.vertx.mqtt.MqttEndpoint;
import io.vertx.mqtt.MqttServer;
import io.vertx.mqtt.MqttTopicSubscription;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MqttServerVerticle extends AbstractVerticle{

  private final Logger LOGGER = LogManager.getLogger(MqttServerVerticle.class);

  @Override
  public void start(Promise<Void> startPromise) throws Exception
  {

    MqttServer mqttServer = MqttServer.create(vertx);
    SharedData sharedData = vertx.sharedData();
    EventBus eb = vertx.eventBus();


    Map<String, String> config = new HashMap<>();
    config.put("bootstrap.servers", config().getString("kafka.bootstrap.servers"));
    config.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
    config.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
    config.put("acks", "1");

    // use producer for interacting with Apache Kafka
    //KafkaProducer<String, String> producer = KafkaProducer.create(vertx, config);
    KafkaProducer<String, String> producer = KafkaProducer.createShared(vertx, "the-producer", config);

    mqttServer
      .endpointHandler(endpoint -> {

        // shows main connect info
        LOGGER.debug("MQTT client [" + endpoint.clientIdentifier() + "] request to connect, clean session = " + endpoint.isCleanSession());

        if (endpoint.auth() != null) {
          LOGGER.debug("[username = " + endpoint.auth().getUsername() + ", password = " + endpoint.auth().getPassword() + "]");
          eb.request("redis-get", endpoint.auth().getUsername() + "-" + endpoint.auth().getPassword(), ar->{
            if(ar.succeeded()){
              LOGGER.debug(ar.result().body());


              sharedData.<String, String>getAsyncMap("mqtt-client-devicesecret-clientIdentifier", res -> {
                if (res.succeeded()) {
                  AsyncMap<String, String> map = res.result();
                  LOGGER.debug("share data create mqtt-client-topic success");

                  map.put(endpoint.auth().getPassword(), endpoint.clientIdentifier(), resput->{
                    LOGGER.debug("put data success");
                  });

                } else {
                  // Something went wrong!
                  LOGGER.error("share data create mqtt-client-topic failed");
                }
              });
              LOGGER.debug("[keep alive timeout = " + endpoint.keepAliveTimeSeconds() + "]");

              // accept connection from the remote client
              endpoint.accept(false);
              // handling requests for subscriptions
              endpoint.subscribeHandler(subscribe -> {

                List<MqttQoS> grantedQosLevels = new ArrayList<>();
                for (MqttTopicSubscription s : subscribe.topicSubscriptions()) {
                  LOGGER.debug("Subscription for " + s.topicName() + " with QoS " + s.qualityOfService());
                  grantedQosLevels.add(s.qualityOfService());
                }
                // ack the subscriptions request
                endpoint.subscribeAcknowledge(subscribe.messageId(), grantedQosLevels);

                sharedData.<String, String>getAsyncMap("mqtt-client-topic", res -> {
                  if (res.succeeded()) {
                    AsyncMap<String, String> map = res.result();
                    LOGGER.debug("share data create mqtt-client-topic success");
                    map.put(endpoint.clientIdentifier(), subscribe.topicSubscriptions().get(0).topicName(), resput->{
                      LOGGER.debug("put data success");
                    });
                  } else {
                    // Something went wrong!
                    LOGGER.error("share data create mqtt-client-topic failed");
                  }
                });
              });

              eb.consumer(endpoint.clientIdentifier(), message -> {
                LOGGER.debug("I have received a message: " + message.body());
                LOGGER.debug(endpoint.clientIdentifier());
                sharedData.<String, String>getAsyncMap("mqtt-client-topic", res -> {
                  if (res.succeeded()) {
                    AsyncMap<String, String> map = res.result();
                    LOGGER.debug("share data create mqtt-client-topic success");
                    map.get(endpoint.clientIdentifier(), resput->{
                      endpoint.publish(resput.result(),
                        Buffer.buffer(message.body().toString()),
                        MqttQoS.AT_LEAST_ONCE,
                        false,
                        false);
                      message.reply("OK");
                    });
                  } else {
                    // Something went wrong!
                    message.fail(1,"error");
                    LOGGER.error("share data create mqtt-client-topic failed");
                  }
                });
                // just as example, publish a message on the first topic with requested QoS
              });

              this.mqttConfig(endpoint);

              // handling incoming published messages
              endpoint.publishHandler(message -> {

                LOGGER.debug("server Just received message on [" + message.topicName() + "] payload [" + message.payload() + "] with QoS [" + message.qosLevel() + "]");
                KafkaProducerRecord<String, String> record = KafkaProducerRecord.create(endpoint.auth().getUsername(),  message.topicName() + " " + message.payload() + " " + message.qosLevel());
                producer.send(record, done -> {
                  if (done.succeeded()) {
                    RecordMetadata recordMetadata = done.result();
                    LOGGER.debug("Message " + record.value() + " written on topic=" + recordMetadata.getTopic() +
                      ", partition=" + recordMetadata.getPartition() +
                      ", offset=" + recordMetadata.getOffset());
                  }else {
                    LOGGER.error("kafka write failed");
                  }
                });
                if (message.qosLevel() == MqttQoS.AT_LEAST_ONCE) {
                  endpoint.publishAcknowledge(message.messageId());
                } else if (message.qosLevel() == MqttQoS.EXACTLY_ONCE) {
                  endpoint.publishReceived(message.messageId());
                }
              }).publishReleaseHandler(messageId -> {
                endpoint.publishComplete(messageId);
              });

            }
            else {
              LOGGER.error("redis auth failed");
              return;
            }
          });

        }else{
          LOGGER.error("not auth");
          return;
        }

        if (endpoint.will() != null) {
          LOGGER.debug("[will topic = " + endpoint.will().getWillTopic() + " msg = " +
            " QoS = " + endpoint.will().getWillQos() + " isRetain = " + endpoint.will().isWillRetain() + "]");
        }

      })
      .listen(config().getInteger("mqtt.port"), config().getString("mqtt.url"), ar -> {
        if (ar.succeeded()) {
          LOGGER.info("MQTT server is listening on port " + mqttServer.actualPort());
          startPromise.complete();
        } else {
          LOGGER.error("Error on starting the server" + ar.cause().getMessage());
          startPromise.fail(ar.cause());
        }
      });
  }


  void mqttConfig(MqttEndpoint endpoint){
    // specifing handlers for handling QoS 1 and 2
    endpoint.publishAcknowledgeHandler(messageId -> {
      LOGGER.debug("Received ack for message = " + messageId);
    }).publishReceivedHandler(messageId -> {
      endpoint.publishRelease(messageId);
    }).publishCompletionHandler(messageId -> {
      LOGGER.debug("Received ack for message = " + messageId);
    });

    // handling requests for unsubscriptions
    endpoint.unsubscribeHandler(unsubscribe -> {
      for (String t : unsubscribe.topics()) {
        LOGGER.debug("Unsubscription for " + t);
      }
      // ack the subscriptions request
      endpoint.unsubscribeAcknowledge(unsubscribe.messageId());
    });
    // handling ping from client
    endpoint.pingHandler(v -> {
      LOGGER.debug("Ping received from client");
    });
    // handling disconnect message
    endpoint.disconnectHandler(v -> {
      LOGGER.debug("Received disconnect from client");
    });
    // handling closing connection
    endpoint.closeHandler(v -> {
      LOGGER.debug("Connection closed");
    });
  }

}


