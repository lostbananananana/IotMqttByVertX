package com.fsmart.iot.starter;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(VertxExtension.class)
public class TestMainVerticle {

  @BeforeEach
  void deploy_verticle(Vertx vertx, VertxTestContext testContext) {
    System.out.println();

    vertx.deployVerticle(new MainVerticle(), new DeploymentOptions().setConfig(new JsonObject()
      .put("http.port", 8080)
      .put("kafka.bootstrap.servers", "192.168.30.32:9092")
      .put("redis.path", "redis://192.168.30.32:6379")
      .put("mqtt.port", 1883)
      .put("mqtt.url", "0.0.0.0")
    ), testContext.succeeding(id -> testContext.completeNow()));
  }

  @Test
  void verticle_deployed(Vertx vertx, VertxTestContext testContext) throws Throwable {
    testContext.completeNow();
  }
}
