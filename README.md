# IotMqttByVertX
mqtt client server with kafka and redis


This is iot platform connect with mqtt using VertX. The data that mqtt client upload is sent to Kafka. The authenticate use redis. 
The Verticle number default is 1.

The architecture look like:

![image](https://raw.githubusercontent.com/elevensheep/IotMqttByVertX/master/iotconect.png)


run command like java -jar starter-1.0.0-SNAPSHOT-fat.jar -conf application-conf.json


